<!DOCTYPE html>
<html lang="en-US" dir="ltr">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ===============================================-->
    <!--    Document Title-->
    <!-- ===============================================-->
    <title>majestic | Landing, Ecommerce &amp; Business Templatee</title>


    <!-- ===============================================-->
    <!--    Favicons-->
    <!-- ===============================================-->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>assets/img/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>assets/img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/img/favicons/favicon-16x16.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/img/favicons/favicon.ico">
    <link rel="manifest" href="<?php echo base_url();?>assets/img/favicons/manifest.json">
    <meta name="msapplication-TileImage" content="<?php echo base_url();?>assets/img/favicons/mstile-150x150.png">
    <meta name="theme-color" content="#ffffff">


    <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link href="<?php echo base_url();?>assets/css/theme.css" rel="stylesheet" />

  </head>


  <body>

    <!-- ===============================================-->
    <!--    Main Content-->
    <!-- ===============================================-->
    <main class="main" id="top">
    <div style="margin-bottom:100px;">
      <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3 d-block" data-navbar-on-scroll="data-navbar-on-scroll">
        <div class="container"><a class="navbar-brand d-inline-flex" href="index.html"><img class="d-inline-block" src="<?php echo base_url();?>assets/img/gallery/logo.png" alt="logo" /><span class="text-1000 fs-0 fw-bold ms-2">Majestic</span></a>
          <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
          <div class="collapse navbar-collapse border-top border-lg-0 mt-4 mt-lg-0" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item px-2"><a class="nav-link fw-medium active" aria-current="page" href="#categoryWomen">Women</a></li>
              <li class="nav-item px-2"><a class="nav-link fw-medium" href="#header">Ajout Produit</a></li>
              <li class="nav-item px-2"><a class="nav-link fw-medium" href="#collection">Collection</a></li>
              <li class="nav-item px-2"><a class="nav-link fw-medium" href="#outlet">Outlet</a></li>
            </ul>
            <form class="d-flex"><a class="text-1000" href="#!">
                <svg class="feather feather-phone me-3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                  <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                </svg></a><a class="text-1000" href="#!">
                <svg class="feather feather-shopping-cart me-3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                  <circle cx="9" cy="21" r="1"></circle>
                  <circle cx="20" cy="21" r="1"></circle>
                  <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path>
                </svg></a><a class="text-1000" href="#!">
                <svg class="feather feather-search me-3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                  <circle cx="11" cy="11" r="8"></circle>
                  <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                </svg></a><a class="text-1000" href="#!">
                <svg class="feather feather-user me-3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                  <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                  <circle cx="12" cy="7" r="4"></circle>
                </svg></a><a class="text-1000" href="#!">
                <svg class="feather feather-heart me-3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                  <path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path>
                </svg></a></form>
          </div>
        </div>
      </nav>
    </div>
                        <div class="carousel slide" id="carouselCategoryWTshirt" data-bs-touch="false" data-bs-interval="false">
                          <div class="carousel-inner">
                            <div class="carousel-item active" data-bs-interval="10000">
                              <div class="row h-100 align-items-center g-2">
                                <!-- eto no manomboka ny boucle -->
                                <?php for($i=0;$i<count($demande);$i++) {?>
                                    <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                      <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="<?php echo base_url(); ?>assets/img/gallery/purple-tshirt.png" alt="..." />
                                      <div class="card-body ps-0 bg-200">
                                        <h5 class="fw-bold text-1000 text-truncate"><?php echo $demande[$i]['nom']; ?></h5>
                                        <h5 class="fw-bold text-1000 text-truncate"><?php echo $demande[$i]['titre']; ?></h5>
                                        <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$<?php echo $demande[$i]['prix']; ?></span></div>
                                      </div>
                                    </div>
                                    
                                    <button type="button" id="popup" class="btn btn-primary" data-toggle="modal" data-target="#eventModal<?php echo $i; ?>" style="background-color:rgb(94, 228, 94);color:white;">
                                      Accepter
                                    </button>
                                    
                                    <div class="col-5 justify-content-center mt-1" style="float:right;"> <a class="btn btn-st btn-dark" href="#!">Refuser</a></div>
                                  </div>
                                  
                                  <?php } ?>
                                </div>
                              </div>
                              
                            </div>
                          </div>
                          <!-- eto le zavatra mipopotra (popup)-->
                          <!-- miaraka amin'ilay boucle ao ambony ito ray ito-->
                          <?php for($i=0;$i<count($demande);$i++) {?>
                          <div class="modal fade" id="eventModal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="eventModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="varyModalLabel">Bouton<?php echo $i; ?></h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body p-4">
                                  <div style="margin-left:80px;">
                                    <img width="281" height="281" src="<?php echo base_url();?>assets/img/gallery/purple-tshirt.png">
                                  </div>
                                  <h5>Etes vous sur de vouloir echanger votre objet avec celui de <?php echo $demande[$i]['nom']; ?>?</h5>
                                  <br>
                                  
                                  <a href = "<?php echo base_url(); ?>acceuil/confirmationEchange?idclient1=<?php echo $demande[$i]['idclient1']; ?>&idclient2=<?php echo $demande[$i]['idclient2']; ?>&idobjet1=<?php echo $demande[$i]['idobjet1']; ?>&idobjet2=<?php echo $demande[$i]['idobjet2']; ?>"><button style="float:left" type="submit" class="btn mb-2 btn-outline-info">Oui</button></a>
                                  <button style="float:right" type="submit" class="btn mb-2 btn-outline-info">Non</button> 
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                        <?php } ?>
                        <!-- de eto no mifarana ny boucle anleh izy teo-->
                        
                        <!-- new event modal -->
                        <section class="py-0 pt-7">

                          <div class="container">
                            <div class="row">
                              <div class="col-6 col-lg-2 mb-3">
                                <h5 class="lh-lg fw-bold text-1000">Company Info</h5>
                                <ul class="list-unstyled mb-md-4 mb-lg-0">
                                  <li class="lh-lg"><a class="text-800 text-decoration-none" href="#!">About Us</a></li>
                                  <li class="lh-lg"><a class="text-800 text-decoration-none" href="#!">Affiliate</a></li>
                                  <li class="lh-lg"><a class="text-800 text-decoration-none" href="#!">Fashion Blogger</a></li>
                                </ul>
                              </div>
                              <div class="col-6 col-lg-2 mb-3">
                                <h5 class="lh-lg fw-bold text-1000">Help &amp; Support</h5>
                                <ul class="list-unstyled mb-md-4 mb-lg-0">
                                  <li class="lh-lg"><a class="text-800 text-decoration-none" href="#!">Shipping Info</a></li>
                                  <li class="lh-lg"><a class="text-800 text-decoration-none" href="#!">Refunds</a></li>
                                  <li class="lh-lg"><a class="text-800 text-decoration-none" href="#!">How to Order</a></li>
                                  <li class="lh-lg"><a class="text-800 text-decoration-none" href="#!">How to Track</a></li>
                                  <li class="lh-lg"><a class="text-800 text-decoration-none" href="#!">Size Guides</a></li>
                                </ul>
                              </div>
                              <div class="col-6 col-lg-2 mb-3">
                                <h5 class="lh-lg fw-bold text-1000">Customer Care</h5>
                                <ul class="list-unstyled mb-md-4 mb-lg-0">
                                  <li class="lh-lg"><a class="text-800 text-decoration-none" href="#!">Contact Us</a></li>
                                  <li class="lh-lg"><a class="text-800 text-decoration-none" href="#!">Payment Methods</a></li>
                                  <li class="lh-lg"><a class="text-800 text-decoration-none" href="#!">Bonus Point</a></li>
                                </ul>
                              </div>
                              <div class="col-sm-6 col-lg-auto ms-auto">
                                <h5 class="lh-lg fw-bold text-1000">Signup For The Latest News</h5>
                                <div class="row input-group-icon mb-5">
                                  <div class="col-12">
                                    <input class="form-control input-box" type="email" placeholder="Enter Email" aria-label="email" />
                                    <svg class="bi bi-arrow-right-short input-box-icon" xmlns="http://www.w3.org/2000/svg" width="23" height="23" fill="#424242" viewBox="0 0 16 16">
                                      <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"> </path>
                                    </svg>
                                  </div>
                                </div>
                                <p class="text-800">
                                  <svg class="feather feather-phone me-3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                                  </svg><span class="text-800">+3930219390</span>
                                </p>
                                <p class="text-800">
                                  <svg class="feather feather-mail me-3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                                    <polyline points="22,6 12,13 2,6"></polyline>
                                  </svg><span class="text-800">something@gmail.com</span>
                                </p>
                              </div>
                            </div>
                            <div class="border-bottom border-3"></div>
                            <div class="row flex-center my-3">
                              <div class="col-md-6 order-1 order-md-0">
                                <p class="my-2 text-1000 text-center text-md-start"> Made with&nbsp;
                                  <svg class="bi bi-suit-heart-fill" xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="#EB6453" viewBox="0 0 16 16">
                                    <path d="M4 1c2.21 0 4 1.755 4 3.92C8 2.755 9.79 1 12 1s4 1.755 4 3.92c0 3.263-3.234 4.414-7.608 9.608a.513.513 0 0 1-.784 0C3.234 9.334 0 8.183 0 4.92 0 2.755 1.79 1 4 1z"></path>
                                  </svg>&nbsp;by&nbsp;<a class="text-800" href="https://themewagon.com/" target="_blank">ThemeWagon </a>
                                </p>
                              </div>
                              <div class="col-md-6">
                                <div class="text-center text-md-end"><a href="#!"><span class="me-4" data-feather="facebook"></span></a><a href="#!"> <span class="me-4" data-feather="instagram"></span></a><a href="#!"> <span class="me-4" data-feather="youtube"></span></a><a href="#!"> <span class="me-4" data-feather="twitter"></span></a></div>
                              </div>
                            </div>
                          </div>
                          <!-- end of .container-->
                  
                        </section>
                        <!-- <section> close ============================-->
                        <!-- ============================================-->
                        
    </main>
    <!-- ===============================================-->
    <!--    End of Main Content-->
    <!-- ===============================================-->




    <!-- ===============================================-->
    <!--    JavaScripts-->
    <!-- ===============================================-->
    <script src="<?php echo base_url();?>js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>js/popper.min.js"></script>
    <script src="<?php echo base_url();?>js/moment.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/simplebar.min.js"></script>
    <script src='<?php echo base_url();?>js/daterangepicker.js'></script>
    <script src='<?php echo base_url();?>js/jquery.stickOnScroll.js'></script>
    <script src="<?php echo base_url();?>js/tinycolor-min.js"></script>
    <script src="<?php echo base_url();?>js/config.js"></script>
    <script src='<?php echo base_url();?>js/jquery.dataTables.min.js'></script>
    <script src='<?php echo base_url();?>js/dataTables.bootstrap4.min.js'></script>
    <script>
      $('#dataTable-1').DataTable(
      {
        autoWidth: true,
        "lengthMenu": [
          [16, 32, 64, -1],
          [16, 32, 64, "All"]
        ]
      });
    </script>
    <script src="<?php echo base_url();?>js/apps.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56159088-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag()
      {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      gtag('config', 'UA-56159088-1');
    </script>
    <script src="<?php echo base_url();?>vendors/@popperjs/popper.min.js"></script>
    <script src="<?php echo base_url();?>vendors/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>vendors/is/is.min.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>
    <script src="<?php echo base_url();?>vendors/feather-icons/feather.min.js"></script>
    <script>
      feather.replace();
    </script>
    <script src="<?php echo base_url();?>assets/js/theme.js"></script>

    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@200;300;400;500;600;700;800;900&amp;display=swap" rel="stylesheet">
  </body>

</html>