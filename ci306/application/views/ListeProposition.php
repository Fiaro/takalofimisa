<!DOCTYPE html>
<html lang="en-US" dir="ltr">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <!-- ===============================================-->
  <!--    Document Title-->
  <!-- ===============================================-->
  <title>majestic | Landing, Ecommerce &amp; Business Templatee</title>


  <!-- ===============================================-->
  <!--    Favicons-->
  <!-- ===============================================-->
  <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicons/favicon-16x16.png">
  <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicons/favicon.ico">
  <link rel="manifest" href="assets/img/favicons/manifest.json">
  <meta name="msapplication-TileImage" content="assets/img/favicons/mstile-150x150.png">
  <meta name="theme-color" content="#ffffff">


  <!-- ===============================================-->
  <!--    Stylesheets-->
  <!-- ===============================================-->
  <link href="assets/css/theme.css" rel="stylesheet">

</head>


<body>


  <!-- ===============================================-->
  <!--    Main Content-->
  <!-- ===============================================-->
  <main class="main" id="top">
    <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3 d-block backdrop shadow-transition" data-navbar-on-scroll="data-navbar-on-scroll" style="background-image: none; background-color: rgba(255, 255, 255, 0.75); transition: none 0s ease 0s;">
      <div class="container"><a class="navbar-brand d-inline-flex" href="index.html"><img class="d-inline-block" src="assets/img/gallery/logo.png" alt="logo"><span class="text-1000 fs-0 fw-bold ms-2">Majestic</span></a>
        <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse border-top border-lg-0 mt-4 mt-lg-0" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item px-2"><a class="nav-link fw-medium active" aria-current="page" href="#categoryWomen">Women</a></li>
            <li class="nav-item px-2"><a class="nav-link fw-medium" href="#header">Ajout Produit</a></li>
            <li class="nav-item px-2"><a class="nav-link fw-medium" href="#collection">Collection</a></li>
            <li class="nav-item px-2"><a class="nav-link fw-medium" href="#outlet">Outlet</a></li>
          </ul>
          <form class="d-flex"><a class="text-1000" href="#!">
              <svg class="feather feather-phone me-3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
              </svg></a><a class="text-1000" href="#!">
              <svg class="feather feather-shopping-cart me-3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                <circle cx="9" cy="21" r="1"></circle>
                <circle cx="20" cy="21" r="1"></circle>
                <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path>
              </svg></a><a class="text-1000" href="#!">
              <svg class="feather feather-search me-3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                <circle cx="11" cy="11" r="8"></circle>
                <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
              </svg></a><a class="text-1000" href="#!">
              <svg class="feather feather-user me-3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                <circle cx="12" cy="7" r="4"></circle>
              </svg></a><a class="text-1000" href="#!">
              <svg class="feather feather-heart me-3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                <path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path>
              </svg></a></form>
        </div>
      </div>
    </nav>


    
    <section id="categoryWomen">
      <div class="container">
        <div class="row h-100">
          <div class="col-lg-7 mx-auto text-center mb-6">
            <h5 class="fw-bold fs-3 fs-lg-5 lh-sm mb-3">Shop By Category</h5>
          </div>
          <div class="col-12">
            <nav>
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-women" role="tabpanel" aria-labelledby="nav-women-tab">
                  <div class="tab-content" id="pills-tabContentWomen">
                    <div class="tab-pane fade" id="pills-dresses" role="tabpanel" aria-labelledby="pills-dresses-tab">
                      <div class="carousel slide" id="carouselCategoryDresses" data-bs-touch="false" data-bs-interval="false">
                        <div class="carousel-inner">
                          <div class="carousel-item active" data-bs-interval="10000">
                            <div class="row h-100 align-items-center g-2">
                                  <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                    <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/shirt-1.png" alt="...">
                                      <div class="card-img-overlay ps-0"> </div>
                                      <div class="card-body ps-0 bg-200">
                                        <h5 class="fw-bold text-1000 text-truncate">Shirt</h5>
                                        <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                      </div><a class="stretched-link" href="#"></a>
                                    </div>
                                  </div>
                            </div>
                          </div>
                          <div class="carousel-item" data-bs-interval="5000">
                            <div class="row h-100 align-items-center g-2">
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/shirt-1.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/shirt-2.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Gray Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/shirt-3.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">White Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/shirt-4.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Black Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$500</span><span class="text-primary">$275</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="carousel-item" data-bs-interval="3000">
                            <div class="row h-100 align-items-center g-2">
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/shirt-1.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/shirt-2.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Gray Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/shirt-3.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">White Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/shirt-4.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Black Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$500</span><span class="text-primary">$275</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="carousel-item">
                            <div class="row h-100 align-items-center g-2">
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/shirt-1.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/shirt-2.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Gray Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/shirt-3.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">White Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/shirt-4.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Black Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$500</span><span class="text-primary">$275</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselCategoryDresses" data-bs-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="visually-hidden">Previous</span></button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselCategoryDresses" data-bs-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="visually-hidden">Next </span></button>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 d-flex justify-content-center mt-5"> <a class="btn btn-lg btn-dark" href="#!">View All </a></div>
                    </div>
                    <div class="tab-pane fade show active" id="pills-wtshirt" role="tabpanel" aria-labelledby="pills-wtshirt-tab">
                      <div class="carousel slide" id="carouselCategoryWTshirt" data-bs-touch="false" data-bs-interval="false">
                        <div class="carousel-inner">
                          <div class="carousel-item active" data-bs-interval="10000">
                            <div class="row h-100 align-items-center g-2">
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/red-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Red T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/pink-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Pink T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/orange-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Orange T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/purple-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Purple T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="carousel-item" data-bs-interval="5000">
                            <div class="row h-100 align-items-center g-2">
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/red-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Red T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/pink-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Pink T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/orange-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Orange T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/purple-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Purple T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="carousel-item" data-bs-interval="3000">
                            <div class="row h-100 align-items-center g-2">
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/red-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Red T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/pink-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Pink T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/orange-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Orange T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/purple-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Purple T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="carousel-item">
                            <div class="row h-100 align-items-center g-2">
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/red-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Red T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/pink-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Pink T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/orange-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Orange T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-3 mb-3 mb-md-0 h-100">
                                <div class="card card-span h-100 text-white"><img class="img-fluid h-100" src="assets/img/gallery/purple-tshirt.png" alt="...">
                                  <div class="card-img-overlay ps-0"> </div>
                                  <div class="card-body ps-0 bg-200">
                                    <h5 class="fw-bold text-1000 text-truncate">Purple T-Shirt</h5>
                                    <div class="fw-bold"><span class="text-600 me-2 text-decoration-line-through">$200</span><span class="text-primary">$175</span></div>
                                  </div><a class="stretched-link" href="#"></a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselCategoryWTshirt" data-bs-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="visually-hidden">Previous</span></button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselCategoryWTshirt" data-bs-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="visually-hidden">Next </span></button>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 d-flex justify-content-center mt-5"> <a class="btn btn-lg btn-dark" href="#!">View All </a></div>
                    </div>
                  </div>
                </div>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </section>


    <!-- ============================================-->
    <!-- <section> begin ============================-->
    
    <!-- <section> close ============================-->
    <!-- ============================================-->


    


    <!-- ============================================-->
    <!-- <section> begin ============================-->
             
    <!-- <section> close ============================-->
    <!-- ============================================-->




    <!-- ============================================-->
    <!-- <section> begin ============================-->
    
    <!-- <section> close ============================-->
    <!-- ============================================-->




    <!-- ============================================-->
    <!-- <section> begin ============================-->
 


  </main>
  <!-- ===============================================-->
  <!--    End of Main Content-->
  <!-- ===============================================-->




  <!-- ===============================================-->
  <!--    JavaScripts-->
  <!-- ===============================================-->
  <script src="vendors/@popperjs/popper.min.js"></script>
  <script src="vendors/bootstrap/bootstrap.min.js"></script>
  <script src="vendors/is/is.min.js"></script>
  <script src="https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>
  <script src="vendors/feather-icons/feather.min.js"></script>
  <script>
    feather.replace();
  </script>
  <script src="assets/js/theme.js"></script>

  <link href="https://fonts.googleapis.com/css2?family=Jost:wght@200;300;400;500;600;700;800;900&amp;display=swap" rel="stylesheet">





</body>

</html>