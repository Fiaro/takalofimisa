<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <title>Login</title>
    <!-- Simple bar CSS -->
    <link rel="stylesheet" href="css/simplebar.css">
    <!-- Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@0,100;0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Icons CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/feather.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/dataTables.bootstrap4.css">
    <!-- Date Range Picker CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/daterangepicker.css">
    <!-- App CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/app-light.css" id="lightTheme" disabled>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/app-dark.css" id="darkTheme">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/styleFiaro.css" id="darkTheme">
  </head>
  <body class="vertical  dark  ">
    <div class="wrapper">

      <main role="main">
        <div id="formulaire" style="margin-top: 100px;">
            <h2>Incription</h2>
            <form action="<?php echo base_url('inscription/traitementInscription/'); ?>" method="post">
              <div class="form-group mb-3">
                <label for="simpleinput">Titre :</label>
                <input name="nom" type="text" id="simpleinput" class="form-control">
              </div>
              <div class="form-group mb-3">
                <label for="example-email">Description :</label>
                <input type="mail" id="example-email" name="email" class="form-control">
              </div>
              <div class="form-group mb-3">
                <label for="example-email">Prix :</label>
                <input type="password" id="example-password" name="mdp" class="form-control">
              </div>
              <br>
              <div class="mb-2">
                <button type="submit" id="bt2" class="btn mb-2 btn-outline-success">Valider</button>
              </div>
            </form>
          </div> 
      </main> <!-- main -->
    </div> <!-- .wrapper -->
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/moment.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/simplebar.min.js"></script>
    <script src='js/daterangepicker.js'></script>
    <script src='js/jquery.stickOnScroll.js'></script>
    <script src="js/tinycolor-min.js"></script>
    <script src="js/config.js"></script>
    <script src='js/jquery.dataTables.min.js'></script>
    <script src='js/dataTables.bootstrap4.min.js'></script>
    <script>
      $('#dataTable-1').DataTable(
      {
        autoWidth: true,
        "lengthMenu": [
          [16, 32, 64, -1],
          [16, 32, 64, "All"]
        ]
      });
    </script>
    <script src="js/apps.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56159088-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag()
      {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      gtag('config', 'UA-56159088-1');
    </script>
  </body>
</html>