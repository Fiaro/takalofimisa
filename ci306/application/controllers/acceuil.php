<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class acceuil extends CI_Controller{
    
    public function pageInscription()
    {
        $this->load->view('inscription');
    }

    public function passer()
    {
        $this->load->view('gestionCategorie');
    }


    public function pageDemandeEchange()
    {
        session_start();
        $id=$_SESSION['id'];
        $this->load->model('echangeModel');
        $demande=array();
        $demande=$this->echangeModel->getProposition($id);
        $data['demande']=$demande;
        $this->load->view('echange',$data);
    }

    public function confirmationEchange()
    {
        $idClient1= $this->input->get('idclient1');
        $idClient2= $this->input->get('idclient2');
        $idobjet1= $this->input->get('idobjet1');
        $idobjet2= $this->input->get('idclient2');
        $this->load->model('echangeModel');
        $this->echangeModel->accepte($idClient1,$idobjet1,$idClient2,$idobjet2);
        redirect(base_url('verifLogin/passerClient'));
    }

    public function recherche()
    {
        session_start();
        $idOwn= $_SESSION['id'];
        $idcategorie=$this->input->post('categorie');
        $keyWord=$this->input->post('keyWord'); 
        $result=array();
        $this->load->model('recherche');
        if($idcategorie==0)
        {
            $result=$this->recherche->rechercheKeyWord($keyWord);
        }
        else if($keyWord==null)
        {
            $result=$this->recherche->rechercheCategorise($idcategorie);
        }
        else
        {
            $result=$this->recherche->rechercheMulti($idcategorie,$keyWord);
        }
        $data['objet']=$result;
        $this->load->model("propositionModel");
        $objet2=array();
        $objet2=$this->propositionModel->getMyObjet($idOwn);
        $data['myObjet']=$objet2;
        $this->load->view('propositionObjets',$data);
    }

}


?>