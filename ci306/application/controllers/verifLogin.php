<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class verifLogin extends CI_Controller{
    
    public function pageLogin()
    {
        $this->load->view('login');
    }
    public function pageLoginClient()
    {
        $this->load->view('loginClient');
    }

    public function passer()
    {
        $this->load->model("gestionCategorieModel");
        $allCategorie=array();
        $allCategorie=$this->gestionCategorieModel->getCategorie();
        $data['categorie']=$allCategorie;
        $this->load->view('gestionCategorie',$data);
    }

    public function passerClient()
    {
        session_start();
        $idOwn=$_SESSION['id'];
        $this->load->model("propositionModel");
        $objet=array();
        $objet=$this->propositionModel->getAllObjet();
        $data['objet']=$objet;
        $objet2=array();
        $objet2=$this->propositionModel->getMyObjet($idOwn);
        $data['myObjet']=$objet2;
        $this->load->view('propositionObjets',$data);
    }


    public function verificationLogin()
    {
        $email= $this->input->post('email');
        $mdp= $this->input->post('mdp');
    
        $this->load->model('LoginModel');
        $data=array();
        $data=$this->LoginModel->getAdmin($email,$mdp);

        if($data==null)
        {
            redirect(base_url('verifLogin/pageLogin'));
        }
        else
        {
            session_start();
            $_SESSION['name']=$data[0]['nom'];
            $_SESSION['id']=$data[0]['idAdmin'];
            redirect(base_url('veriflogin/passer'));
        }
    }

    public function verificationLoginClient()
    {
        $email= $this->input->post('email');
        $mdp= $this->input->post('mdp');
    
        $this->load->model('LoginModel');
        $data=array();
        $data=$this->LoginModel->getClient($email,$mdp);

        if($data==null)
        {
            redirect(base_url('verifLogin/pageLoginClient'));
        }
        else
        {
            session_start();
            $_SESSION['name']=$data[0]['nom'];
            $_SESSION['id']=$data[0]['idClient'];
            redirect(base_url('veriflogin/passerClient'));
        }
    }

}


?>