<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class echange extends CI_Controller{
    
    public function pageInscription()
    {
        $this->load->view('inscription');
    }

    public function passer()
    {
        $this->load->view('gestionCategorie');
    }


    public function envoiDemande()
    {
        $idClient1= $this->input->get('idclient1');
        $idClient2= $this->input->get('idclient2');
        $idobjet1= $this->input->get('idobjet1');
        $idobjet2= $this->input->get('idclient2');
        $this->load->model('echangeModel');
        $this->echangeModel->insertEchange($idClient1,$idobjet1,$idClient2,$idobjet2);
        redirect(base_url('veriflogin/passerClient'));
    }

    public function confirmationEchange()
    {
        /*$idClient1= $this->input->post('idclient1');
        $idClient2= $this->input->post('idclient2');
        $idobjet1= $this->input->post('idobjet1');
        $idobjet2= $this->input->post('idclient2');
        $this->load->model('echangeModel');
        $this->echangeModel->accepte($idClient1,$idobjet1,$idClient2,$idobjet2);*/
        redirect(base_url('verifLogin/passerClient#outlet'));
    }

}


?>