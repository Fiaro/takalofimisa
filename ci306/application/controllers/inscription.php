<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class inscription extends CI_Controller{
    
    public function pageInscription()
    {
        $this->load->view('inscription');
    }

    public function passer()
    {
        $this->load->view('gestionCategorie');
    }


    public function traitementInscription()
    {
        $nom= $this->input->post('nom');
        $email= $this->input->post('email');
        $mdp= $this->input->post('mdp');

    
        $this->load->model('inscriptionClient');
        $this->inscriptionClient->setClient($nom,$email,$mdp);

        redirect(base_url('verifLogin/pageLogin'));
    }

}


?>