<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class recherche extends CI_Model{
    
    public function rechercheCategorise($categorie)
    {
        $query = $this->db->query("select * from objet join client on objet.idclient=client.idclient join photo on photo.idobjet=objet.idobjet where idcategorie=$categorie");
        $result = array();
        foreach($query->result_array() as $row)
        {
            array_push($result,$row);
        }
        return $result;
    }

    public function rechercheKeyWord($keyWord)
    {
        $query = $this->db->query("select * from objet join client on objet.idclient=client.idclient join photo on photo.idobjet=objet.idobjet where description like '%$keyWord%'");
        $result = array();
        foreach($query->result_array() as $row)
        {
            array_push($result,$row);
        }
        return $result;
    }

    public function rechercheMulti($categorie,$keyWord)
    {
        $query = $this->db->query("select * from objet join client on objet.idclient=client.idclient join photo on photo.idobjet=objet.idobjet where description like '%$keyWord%' and idcategorie=$categorie");
        $result = array();
        foreach($query->result_array() as $row)
        {
            array_push($result,$row);
        }
        return $result;
    }
}


?>