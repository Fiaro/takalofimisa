<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class gestionCategorieModel extends CI_Model{
    
    public function getCategorie()
    {
        $query = $this->db->query("select * from categorie");
        $result = array();
        foreach($query->result_array() as $row)
        {
            array_push($result,$row);
        }
        return $result;
    }

    public function setCategorie($nom)
    {
        $sql="insert into categorie values (null,%s)";
        $sql=sprintf($sql,$this->db->escape($nom));
        $this->db->query($sql);
        redirect(base_url('verifLogin/passer'));
    }

    public function supprCategorie($id)
    {
        $sql="delete from categorie where idCategorie=%d";
        $sql=sprintf($sql,$id);
        $this->db->query($sql);
        redirect(base_url('verifLogin/passer'));
    }
}


?>