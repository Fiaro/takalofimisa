CREATE database takalo;

USE takalo;

CREATE TABLE client (
  idClient int AUTO_INCREMENT,
  nom varchar(50),
  email varchar(50),
  password varchar(50),
  PRIMARY KEY (idClient)

) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `categorie` (
  `idCategorie` int AUTO_INCREMENT,
  `nom` varchar(50),
  PRIMARY KEY (idCategorie)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `objet` (
  `idObjet` int AUTO_INCREMENT,
  `titre` varchar(50),
  `description` varchar(100),
  `prix` int,
  `idClient` int,
  `idCategorie` int,  
  PRIMARY KEY (idObjet),
  FOREIGN KEY (idClient) REFERENCES client(idClient),
  FOREIGN KEY (idCategorie) REFERENCES categorie(idCategorie)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `admin` (
  `idAdmin` int AUTO_INCREMENT,
  `nom` varchar(50),
  `email` varchar(50),
  `password` varchar(50),
  PRIMARY KEY (idAdmin)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `photo` (
  `idObjet` int,
  `photo` varchar(50),
  PRIMARY KEY (idObjet)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `echange` (
  `idClient1` int,
  `idClient2` int,
  `idObjet1` int,
  `idObjet2` int,
  `accepte1` int DEFAULT 0,
  `accepte2` int DEFAULT 0,
  FOREIGN KEY (idClient1) REFERENCES client(idClient),
  FOREIGN KEY (idClient2) REFERENCES client(idClient),
  FOREIGN KEY (idObjet1) REFERENCES objet(idObjet),
  FOREIGN KEY (idObjet2) REFERENCES objet(idObjet)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE historiquePossession (
  idClient int,
  idObjet int,
  id int AUTO_INCREMENT,
  PRIMARY KEY (id),
  FOREIGN KEY (idClient) REFERENCES client(idClient),
  FOREIGN KEY (idObjet) REFERENCES objet(idObjet)   
);