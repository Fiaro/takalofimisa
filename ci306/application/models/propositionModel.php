<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class propositionModel extends CI_Model{
    
    public function getAllObjet()
    {
        $id=$_SESSION['id'];
        $query = $this->db->query("select * from objet join client on objet.idclient=client.idclient where objet.idclient!=$id");
        $result = array();
        foreach($query->result_array() as $row)
        {
            array_push($result,$row);
        }
        return $result;
    }

    public function getMyObjet($id)
    {
        $query = $this->db->query("select * from objet join client on objet.idclient=client.idclient where objet.idclient=$id");
        $result = array();
        foreach($query->result_array() as $row)
        {
            array_push($result,$row);
        }
        return $result;
    }
}


?>